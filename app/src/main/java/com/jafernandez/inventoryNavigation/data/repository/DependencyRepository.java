package com.jafernandez.inventoryNavigation.data.repository;

import android.util.Log;

import com.jafernandez.inventoryNavigation.data.model.Dependency;

import java.util.ArrayList;
import java.util.List;

public class DependencyRepository {
    private List<Dependency> list;
    private static DependencyRepository repository;


    /**
     * Se inicializa en el siguiente bloque todas las propiedades estaticas
     * sin tener que realizarlo en un metodo estatico
     * Se evita comprobar si es null
     * */
    static{
        repository= new DependencyRepository();
    }

    private DependencyRepository()
    {
        list = new ArrayList<Dependency>();
        initialice();
    }

    private void initialice() {
        list.add(new Dependency("2º Ciclo formativo Grado Superior",
                "2CFGS", "Aula de Informatica","2018", ""));
        list.add(new Dependency("1º Ciclo formativo Grado Superior",
                "1CFGS", "La otra Aula de Informatica","2018", ""));
        list.add(new Dependency("2º Ciclo formativo Grado Superior",
                "2CFGS", "Aula de Informatica","2019", ""));
        list.add(new Dependency("2º Ciclo formativo Grado Medio",
                "2CFGM", "Aula de Informatica 2","2019", ""));
        list.add(new Dependency("1º Ciclo formativo Grado Medio",
                "1CFGM", "Aula de Informatica 3","2020", ""));
        list.add(new Dependency("1º de ESO",
                "1ESO", "Aula 10","2020", ""));
        list.add(new Dependency("2º de ESO",
                "2ESO", "Aula 20","2018", ""));
        list.add(new Dependency("3º de ESO",
                "3ESO", "Aula 30","2019", ""));
        list.add(new Dependency("4º de ESO",
                "4ESO", "Aula 40","2020", ""));
    }

    public static DependencyRepository getInstance()
    {
        return repository;
    }

    public List<Dependency> getList()
    {
        return list;
    }


    public boolean add(Dependency dependency) {
        return list.add(dependency);

    }

    public void edit(Dependency dependency) {

    int pos = list.indexOf(dependency);
    list.remove(pos);
    list.add(pos,dependency);
        Log.d("BORRAR", "se ha editado");
    }

    public void IfExists(Dependency dependency) {
    }

    public boolean delete(Dependency dependency) {
        return list.remove(dependency);
    }
}
