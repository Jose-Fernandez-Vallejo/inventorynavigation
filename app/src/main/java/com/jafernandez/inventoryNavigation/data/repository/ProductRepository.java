package com.jafernandez.inventoryNavigation.data.repository;

import com.jafernandez.inventoryNavigation.data.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductRepository {
    private static final ProductRepository ourInstance = new ProductRepository();

    public static ProductRepository getInstance() {
        return ourInstance;
    }

    List<Product> list;

        private ProductRepository() {
            list = new ArrayList<>();
            initialice();
    }

    private void initialice() {

    }
}
