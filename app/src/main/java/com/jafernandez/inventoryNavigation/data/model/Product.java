package com.jafernandez.inventoryNavigation.data.model;

import android.graphics.Bitmap;

import java.util.Objects;

public class Product {
    String id;
    String serial;
    String  shortname;
    String modelcode;
    String description;
    int category;
    int subcategory;
    int productClass;
    int sectionId;
    int status;
    double value;
    String vendor;
    Bitmap bitmap;
    String imageBase64;
    String imageName;
    String url;
    String datePurchase;
    String notes;

    public Product() {
    }

    public Product(String id, String serial, String shortname, String modelcode, String description, int category, int subcategory, int productClass, int sectionId, int status, double value, String vendor, Bitmap bitmap, String imageBase64, String imageName, String url, String datePurchase, String notes) {
        this.id = id;
        this.serial = serial;
        this.shortname = shortname;
        this.modelcode = modelcode;
        this.description = description;
        this.category = category;
        this.subcategory = subcategory;
        this.productClass = productClass;
        this.sectionId = sectionId;
        this.status = status;
        this.value = value;
        this.vendor = vendor;
        this.bitmap = bitmap;
        this.imageBase64 = imageBase64;
        this.imageName = imageName;
        this.url = url;
        this.datePurchase = datePurchase;
        this.notes = notes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getModelcode() {
        return modelcode;
    }

    public void setModelcode(String modelcode) {
        this.modelcode = modelcode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(int subcategory) {
        this.subcategory = subcategory;
    }

    public int getProductClass() {
        return productClass;
    }

    public void setProductClass(int productClass) {
        this.productClass = productClass;
    }

    public int getSectionId() {
        return sectionId;
    }

    public void setSectionId(int sectionId) {
        this.sectionId = sectionId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getImageBase64() {
        return imageBase64;
    }

    public void setImageBase64(String imageBase64) {
        this.imageBase64 = imageBase64;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDatePurchase() {
        return datePurchase;
    }

    public void setDatePurchase(String datePurchase) {
        this.datePurchase = datePurchase;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "Product{" +
                "shortname='" + shortname + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
