package com.jafernandez.inventoryNavigation.iu.dependency;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.net.Uri;
import android.os.Bundle;

import com.jafernandez.inventoryNavigation.R;
import com.jafernandez.inventoryNavigation.data.model.Dependency;
import com.jafernandez.inventoryNavigation.iu.base.BaseActivity;

public class DependencyActivity extends BaseActivity implements
        DependencyListFragment.OnManageDependencyListener, DependencyManageFragment.OnFragmentInteractionListener {

    private Fragment dependencyListFragment;
    private DependencyListPresenter dependencyListPresenter;
    private DependencyManageFragment dependencyManageFragment;
    private DependencyManagePresenter dependencyManagePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        showListFragment();

    }

    //muestra el fragment listfragment
    private void showListFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();

        dependencyListFragment = fragmentManager.findFragmentByTag(DependencyListFragment.TAG);

        if(dependencyListFragment==null)
        {
            dependencyListFragment = DependencyListFragment.newInstance(null);



            dependencyListPresenter = new DependencyListPresenter((DependencyListContract.View) dependencyListFragment);
            ((DependencyListContract.View) dependencyListFragment).setPresenter(dependencyListPresenter);
            fragmentManager.beginTransaction().add(android.R.id.content,
                    dependencyListFragment, DependencyListFragment.TAG).commit();
        }

    }



    //muestra el fragment DependencyManageFragment
    @Override
    public void onManageDependency(Dependency dependency) {
        showManageFragment(dependency);

    }
    private void showManageFragment(Dependency dependency) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        dependencyManageFragment = (DependencyManageFragment) fragmentManager.findFragmentByTag(DependencyManageFragment.ARG);
        if(dependencyManageFragment ==null)
        {
            Bundle bundle = null;
            if(dependency != null)
            {
                bundle = new Bundle();
                bundle.putParcelable(Dependency.TAG, dependency);
            }
            dependencyManageFragment = (DependencyManageFragment) DependencyManageFragment.newInstance(bundle);
        }

            //depues de crear la vista se crea el presenter
            dependencyManagePresenter = new DependencyManagePresenter(dependencyManageFragment);
            dependencyManageFragment.setPresenter(dependencyManagePresenter);


            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(android.R.id.content,
                    dependencyManageFragment, DependencyManageFragment.ARG);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();


        }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


}

