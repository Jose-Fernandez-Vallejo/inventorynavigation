package com.jafernandez.inventoryNavigation.iu.base;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class BaseDialogFragment extends DialogFragment {
    public static final String TITLE = "title";
    public static final String MESSAGE = "message";
    public static final String TAG = "BaseDialogFragment";


    //metodo callback del listener del DialogFragment
    //cuando se pulsa aceptar
    public interface OnFinishDialogListener
    {
        void onFinishDialog();
    }


    public static BaseDialogFragment newInstance(Bundle bundle)
    {
        BaseDialogFragment dialogFragment = new BaseDialogFragment();
        if(bundle != null)
            dialogFragment.setArguments(bundle);

        return dialogFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        String title = getArguments().getString(TITLE);
        String message = getArguments().getString(MESSAGE);
        //Se crea el cuadro de dialogo usando el patron CREATOR/BUILDER
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(getString(android.R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                OnFinishDialogListener listener = (OnFinishDialogListener)getTargetFragment();
                listener.onFinishDialog();
            }
        });
        builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        return builder.create();
    }
}
