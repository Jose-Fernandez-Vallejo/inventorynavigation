package com.jafernandez.inventoryNavigation.iu.seccion;

import com.jafernandez.inventoryNavigation.data.model.Seccion;
import com.jafernandez.inventoryNavigation.data.repository.SeccionRepository;

public class SeccionManagePresenter implements SeccionManageContract.Presenter {

    SeccionManageContract.View view;
    public SeccionManagePresenter(SeccionManageContract.View view)
    {
        this.view = view;
    }

    @Override
    public void validateSeccion(Seccion seccion) {
        view.onSuccessValidate();
    }

    @Override
    public void add(Seccion seccion) {
        SeccionRepository.getInstance().add(seccion);
    view.onSuccess();
    }

    @Override
    public void edit(Seccion seccion) {
        SeccionRepository.getInstance().edit(seccion);
    view.onSuccess();
    }
}
