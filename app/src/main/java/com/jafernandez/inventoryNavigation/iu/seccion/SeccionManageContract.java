package com.jafernandez.inventoryNavigation.iu.seccion;

import com.jafernandez.inventoryNavigation.data.model.Seccion;
import com.jafernandez.inventoryNavigation.iu.base.BaseView;

public interface SeccionManageContract {
    interface View extends BaseView<Presenter>
    {
        void onSuccessValidate();
    }
    interface Presenter
    {
        void validateSeccion(Seccion seccion);
        void add(Seccion seccion);
        void edit(Seccion seccion);
    }

}
