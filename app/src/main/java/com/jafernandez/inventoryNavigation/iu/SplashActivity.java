package com.jafernandez.inventoryNavigation.iu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.jafernandez.inventoryNavigation.R;

public class SplashActivity extends AppCompatActivity {
    static final long WAIT_TIME=3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    //  runnable (dentro de un thread) ejecuta el run fuera del hilo de la Interfaz grafica
    @Override
    protected void onStart() {
        super.onStart();
        Handler handler = new Handler();


        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                initLogin();
            }
        };
        handler.postDelayed(runnable, WAIT_TIME);


        //No es una buena opcion
        /*Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(WAIT_TIME);
                    initLogin();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(runnable).start();*/

    }

    private void initLogin()
    {
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
