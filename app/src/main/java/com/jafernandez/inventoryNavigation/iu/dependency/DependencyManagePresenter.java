package com.jafernandez.inventoryNavigation.iu.dependency;

import com.jafernandez.inventoryNavigation.data.model.Dependency;
import com.jafernandez.inventoryNavigation.data.repository.DependencyRepository;

public class DependencyManagePresenter implements DependencyManageContract.Presenter {

    private DependencyManageContract.View view;

    public DependencyManagePresenter(DependencyManageContract.View view)
    {
        this.view = view;
    }

    @Override
    public void ifExists(Dependency dependency) {
        DependencyRepository.getInstance().IfExists(dependency);
        view.onSuccess();
    }

    /**
     * Este metodo valida RND2,RND3,RND4
     * @param dependency
     */
    @Override
    public void validateDependency(Dependency dependency)
    {
        view.onSuccessValidate();
       // view.showError("Error Validando");
    }

    @Override
    public void add(Dependency dependency) {
        DependencyRepository.getInstance().add(dependency);
        view.onSuccess();
    }

    @Override
    public void edit(Dependency dependency) {
        DependencyRepository.getInstance().edit(dependency);
        view.onSuccess();

    }

}
