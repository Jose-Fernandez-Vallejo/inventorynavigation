package com.jafernandez.inventoryNavigation.iu.dependency;

import com.jafernandez.inventoryNavigation.data.model.Dependency;
import com.jafernandez.inventoryNavigation.iu.base.BaseView;

import java.util.List;

public interface DependencyListContract {

    interface View extends BaseView<Presenter>
    {
        void showProgress();
        void hideProgress();
        void showNoData();
        void showData();
        boolean hasData();
        void showSuccess(List<Dependency> dependencyList);
        void OnSuccessDeleted();

        void onSuccessUndo(Dependency dependency);
    }

    interface Presenter
    {
        void delete(Dependency dependency);
        void load();


        void undo(Dependency dependency);
    }

}
