package com.jafernandez.inventoryNavigation.iu.seccion;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.jafernandez.inventoryNavigation.R;
import com.jafernandez.inventoryNavigation.data.model.Seccion;

public class SeccionActivity extends AppCompatActivity implements SeccionListFragment.OnManageSeccionListener {


    SeccionListFragment seccionListFragment;
    SeccionManageFragment seccionManageFragment;
    SeccionManagePresenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seccion);

        showListSeccion();
    }

    private void showListSeccion() {
        FragmentManager fragmentManager = getSupportFragmentManager();

        seccionListFragment = (SeccionListFragment)fragmentManager.findFragmentByTag(SeccionListFragment.TAG);

        if(seccionListFragment==null)
        {
            seccionListFragment = SeccionListFragment.newInstance(null);

            fragmentManager.beginTransaction().add(R.id.SeccionActivity,seccionListFragment, SeccionListFragment.TAG).commit();
        }
    }

    @Override
    public void OnManageSeccion(Seccion seccion) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        seccionManageFragment = (SeccionManageFragment) fragmentManager.findFragmentByTag(SeccionManageFragment.TAG);

        if(seccionManageFragment==null)
        {
            Bundle bundle = null;

            if (seccion!=null)
            {
                bundle = new Bundle();
                bundle.putParcelable(Seccion.TAG,seccion);
            }

            seccionManageFragment = SeccionManageFragment.newInstance(bundle);
            presenter = new SeccionManagePresenter(seccionManageFragment);
            seccionManageFragment.setPresenter(presenter);

        }
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.SeccionActivity,seccionManageFragment, SeccionManageFragment.TAG);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();



    }
}
