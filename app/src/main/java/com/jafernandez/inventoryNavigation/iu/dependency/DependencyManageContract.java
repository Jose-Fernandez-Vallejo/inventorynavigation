package com.jafernandez.inventoryNavigation.iu.dependency;


import com.jafernandez.inventoryNavigation.data.model.Dependency;
import com.jafernandez.inventoryNavigation.iu.base.BaseView;

/**
 * interfaz que corresponde al contrato que se establece entre DependencyManagerFragment (vista)
 * y DependencyManagePresenter(presenter)*/

public class DependencyManageContract {

    interface View extends BaseView<Presenter>
    {
    void onSuccessValidate();

    }

    interface Presenter
    {
        void ifExists(Dependency dependency);
        void validateDependency(Dependency dependency);
        void add(Dependency dependency);
        void edit(Dependency dependency);
    }
}
