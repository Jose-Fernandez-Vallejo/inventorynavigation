package com.jafernandez.inventoryNavigation.iu.base;


//Interfaz base para todas las vistas del proyecto
public interface BaseView<T> {

    void setPresenter (T presenter);


    void showError(String error);

    void onSuccess();
}
