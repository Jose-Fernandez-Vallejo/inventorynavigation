package com.jafernandez.inventoryNavigation.iu.seccion;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jafernandez.inventoryNavigation.R;
import com.jafernandez.inventoryNavigation.adapter.SeccionAdapter;
import com.jafernandez.inventoryNavigation.data.model.Seccion;


public class SeccionListFragment extends Fragment {

    public static final String TAG = "SeccionListFragment";
    private RecyclerView rvSeccion;
    private SeccionAdapter adapter;
    private FloatingActionButton fab;
    private OnManageSeccionListener listener;
    private SeccionAdapter.OnManageSeccionListener listenerAdapter;

    interface OnManageSeccionListener
    {
        void OnManageSeccion(Seccion seccion);
    }

    public SeccionListFragment() {
        // Required empty public constructor
    }


    public static SeccionListFragment newInstance(Bundle bundle) {
        SeccionListFragment fragment = new SeccionListFragment();
        if(bundle!= null)
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_seccion_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvSeccion = view.findViewById(R.id.rvSeccion);
        fab = view.findViewById(R.id.fab);
        initSeccionAdapter();
        initfabIcon();
    }

    private void initfabIcon() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnManageSeccion(null);
            }
        });
    }

    private void initSeccionAdapter() {

        initListenerAdapter();

        adapter = new SeccionAdapter(listenerAdapter);


        rvSeccion.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL,false));
        rvSeccion.setAdapter(adapter);

    }

    private void initListenerAdapter() {
        listenerAdapter = new SeccionAdapter.OnManageSeccionListener() {
            @Override
            public void OnEditSeccion(Seccion seccion) {
                listener.OnManageSeccion(seccion);
            }
        };
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnManageSeccionListener) {
            listener = (OnManageSeccionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
       listener = null;
    }

}
