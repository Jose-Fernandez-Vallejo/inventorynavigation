package com.jafernandez.inventoryNavigation.utils;

import java.util.regex.Pattern;

/**
 * estas clases nose pueden heredar por eso le indicamos el final
 * */

public final class CommonUtils {

    /**
     * Comprueba si el password cumple el patron del modelo de negocio*/
    public static boolean CheckPassword(String pass)
    {
        String regex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=.])(?=\\S+$).{8,12}$";

        Pattern patron = Pattern.compile(regex);
        return patron.matcher(pass).matches();
    }


}
